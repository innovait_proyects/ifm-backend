var express = require('express');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
var mdAutenticacion = require('../middlewares/authentication');
var Usuario = require('../models/usuario');

var app = express();

// ==========================================
// Obtener todos los usuarios
// ==========================================

app.get('/', [mdAutenticacion.verificaToken, mdAutenticacion.verificaADMIN_ROLE], (req, res, next) => {

    var desde = req.query.desde || 0;
    desde = Number(desde);

    Usuario.find({}, 'codigo name apellido email rol grupo_de_trabajo')
        .skip(desde)
        .limit(5)
        .exec(
            (err, usuarios) => {
                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando usuario',
                        errors: err
                    });
                }
                Usuario.count({}, (err, conteo) => {

                    res.status(200).json({
                        ok: true,
                        usuarios: usuarios,
                        total: conteo
                    });

                })
            });
});


// ==========================================
// Actualizar usuario
// ==========================================

app.put('/:id', mdAutenticacion.verificaToken, (req , res) => {

    var id = req.params.id;
    var body = req.body;
    console.log('actualizar usuario   ', body);

    Usuario.findById(id, (err, usuario) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar medico',
                errors: err
            });
        }
        if (!usuario) {
            return res.status(400).json({
                ok: false,
                mensaje: 'El medico con el id ' + id + ' no existe',
                errors: { message: 'No existe un medico con ese ID' }
            });
        }
        usuario.codigo = body.codigo;
        usuario.name = body.name;
        usuario.apellido = body.apellido;
        usuario.email = body.email;
        usuario.rol = body.rol;
        usuario.grupo_de_trabajo = body.grupo_de_trabajo;

        usuario.save((err, usuarioSave) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar medico',
                    errors: err
                });
            }

            res.status(200).json({
                ok: true,
                usuario: usuarioSave
            });

        });

    });

});

// ==========================================
// Crear un nuevo usuario
// ==========================================

app.post('/', (req, res) => {

    var body = req.body;

    var usuario = new Usuario({
        codigo: body.codigo,
        name: body.name,
        apellido: body.apellido,
        email: body.email,
        rol: body.rol,
        grupo_de_trabajo: body.grupo_de_trabajo,
        password: body.password
    });

    usuario.save((err, usuarioGuardado) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear usuario',
                errors: err
            });
        }

        res.status(201).json({
            ok: true,
            usuario: usuarioGuardado,
            usuariotoken: req.usuario
        });


    });

});

// ============================================
//   Borrar un usuario por el id
// ============================================
app.delete('/:id', [mdAutenticacion.verificaToken, mdAutenticacion.verificaADMIN_ROLE], (req , res) => {

    var id = req.params.id;

    Usuario.findByIdAndRemove(id, (err, usuarioBorrado) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error borrar usuario',
                errors: err
            });
        }

        if (!usuarioBorrado) {
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un usuario con ese id',
                errors: { message: 'No existe un usuario con ese id' }
            });
        }

        res.status(200).json({
            ok: true,
            usuario: usuarioBorrado
        });

    });

});

module.exports = app;