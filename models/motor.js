var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var motorSchema = new Schema({
    created_at: {type: String},
    updated_at: {type: String},
    ubicacion: {type: String},
    cliente: {type: String},
    equipo: {type: String},
    desc_elemento: {type: String},
    id_instalacion: {type: Number},
    elemento1:[
        {
            id_elemento: { type:  Schema.Types.ObjectId, ref: 'Elemento1' },
        }
    ]
},{collection: 'motors'})

module.exports = mongoose.model('Motor', motorSchema);