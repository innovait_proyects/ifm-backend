var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var compresorSchema = new Schema({
    created_at: {type: String},
    updated_at: {type: String},
    ubicacion: {type: String},
    cliente: {type: String},
    equipo: {type: String},
    desc_elemento: {type: String},
    id_instalacion: {type: Number},
    elemento2:[
        {
            id_elemento: { type:  Schema.Types.ObjectId, ref: 'Elemento2' },
        }
    ]
},{collection: 'compresors'})

module.exports = mongoose.model('Motor', motorSchema);