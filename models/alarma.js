var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var alarmaSchema = new Schema({
    idelemento: { type: Number },
    fecha: { type: String },
    idalarma: { type: Number },
    descripcion_alarma: { type: String},
    valor: { type: Number},
    estado: { type: Number },
    error: { type: Number },
    startdate: { type: String },
}, { collection: 'alarmas' });



module.exports = mongoose.model('Alarma', alarmaSchema);