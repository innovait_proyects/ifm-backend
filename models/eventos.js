var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var eventoSchema = new Schema({
    observacion:{ type : String},
    accion: { type : String},
    usuarioId: { type: Schema.Types.ObjectId, ref: 'Usuarios' },
    alarmaId: { type: Schema.Types.ObjectId, ref: 'Alarmas'},
    created_at: {type: String},
    updated_at: {type: String}
}, { collection: 'eventos' });



module.exports = mongoose.model('Evento', eventoSchema);