# Proyecto IFM backend 

Parte Nodejs+Expressjs del proyecto de IFM

# Descarga proyecto

Una vez clonado el proyecto lo primero que se debe hacer es desde terminal un npm install para descargar todas las dependencias del proyecto.

# Monitorizar automaticamente los cambios de codigo

Desde consola primero hacer un: tsc -w para monotorizar los cambios de codigo en tiempo real
Se nos compilara el codigo en un directorio ./dist y lanzara la aplicacion desde el archivo app.js de este directorio (Este paso solo es en entorno de desarrollo).

# Levantar el servidor 

Una vez tengamos creada la carpeta dist desde terminal levantamos el servidor desde terminal con: nodemon start